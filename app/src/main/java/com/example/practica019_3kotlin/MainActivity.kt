package com.example.practica019_3kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    //Variables necesarias
    private lateinit var btnSaludar:Button
    private lateinit var btnLimpiar:Button
    private lateinit var btnCerrar:Button
    private lateinit var txtNombre:EditText
    private lateinit var lblSaludo:TextView

    //Relacionar las variables con los elementos de las vistas
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnSaludar = findViewById(R.id.btnSaludar) as Button
        btnLimpiar = findViewById(R.id.btnLimpiar) as Button
        btnCerrar = findViewById(R.id.btnCerrar) as Button
        txtNombre = findViewById(R.id.txtNombre) as EditText
        lblSaludo = findViewById(R.id.lblSaludo) as TextView

        //Codificación del boton Saludar
        btnSaludar.setOnClickListener{
            val str:String

            if (txtNombre.text.toString().contentEquals("")){
                Toast.makeText(applicationContext,"Falso capturar datos", Toast.LENGTH_LONG).show()
            }
            else {
                str = "Hola " + txtNombre.text.toString() + " ¿Como estas? "
                lblSaludo.text = str
            }
        }

        //Codificación del boton limpiar
        btnLimpiar.setOnClickListener{
            lblSaludo.text = ""
            val editable: Editable = Editable.Factory.getInstance().newEditable("")
            txtNombre.text = editable
            txtNombre.requestFocus()
        }

        //Codificación del boton cerrar
        btnCerrar.setOnClickListener{
            finish()
        }
    }
}
